import { IComment } from '../components/DetailPage/CommentsItem/types';
import { IMovieDetail } from '../components/DetailPage/MovieDetail/types';
import { IMovie } from '../components/ListPage/MoviesItem/types';

export interface IMoviesState {
  list: IMovie[],
  isLoading: boolean,
  currentPage: number,
}

export interface IMovieDetailState {
  info: IMovieDetail,
  isLoading: boolean,
}

export interface IRootState {
  movies: IMoviesState,
  comments: IComment[],
  movieDetail: IMovieDetailState,
}

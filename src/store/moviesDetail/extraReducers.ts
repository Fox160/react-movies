import { createAsyncThunk, PayloadAction } from '@reduxjs/toolkit';
import { IMovieDetail } from '../../components/DetailPage/MovieDetail/types';
import fetchMovieDetailHelper from '../../helpers/fetchMovieDetail';
import { IMovieDetailState } from '../types';

export const fetchMovieDetail = createAsyncThunk(
  'movieDetail/getMovie',
  fetchMovieDetailHelper,
);

const extraReducers = (builder: any) => {
  builder.addCase(fetchMovieDetail.pending, (state: IMovieDetailState) => {
    state.isLoading = true;
  }).addCase(fetchMovieDetail.fulfilled,
    (state: IMovieDetailState, action: PayloadAction<IMovieDetail>) => {
      state.info = action.payload;
      state.isLoading = false;
    }).addCase(fetchMovieDetail.rejected, (state: IMovieDetailState) => {
    state.isLoading = false;
  });
};

export default extraReducers;

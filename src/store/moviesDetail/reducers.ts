const reducers = {
  clearMovieDetail: () => ({
    info: {},
    isLoading: false,
  }),
};

export default reducers;

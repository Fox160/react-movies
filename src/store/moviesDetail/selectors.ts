import { IRootState } from '../types';

const selectMovieDetail = (state: IRootState) => state.movieDetail;

export default selectMovieDetail;

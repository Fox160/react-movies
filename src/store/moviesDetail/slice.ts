import { createSlice } from '@reduxjs/toolkit';
import { IMovieDetailState } from '../types';
import extraReducers from './extraReducers';
import reducers from './reducers';

const initialState : IMovieDetailState = {
  info: {},
  isLoading: false,
};

const { actions, reducer } = createSlice({
  name: 'movieDetail',
  initialState,
  reducers,
  extraReducers,
});

export const { clearMovieDetail } = actions;

export default reducer;

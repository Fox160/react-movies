import { PayloadAction } from '@reduxjs/toolkit';
import { IMoviesState } from '../types';

const reducers = {
  setCurrentPage: (state: IMoviesState, action: PayloadAction<number>) => (
    { ...state, currentPage: action.payload }
  ),
};

export default reducers;

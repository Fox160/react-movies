import { createAsyncThunk } from '@reduxjs/toolkit';
import fetchMovieHelper from '../../helpers/fetchMovie';
import { IMoviesState } from '../types';

export const fetchMovies = createAsyncThunk(
  'movies/getMovies',
  fetchMovieHelper,
);

const extraReducers = (builder: any) => {
  builder.addCase(fetchMovies.pending, (state: IMoviesState) => (
    { ...state, isLoading: true }
  )).addCase(fetchMovies.fulfilled, (state: IMoviesState, action: any) => {
    const { list, currentPage } = action.payload;

    return { list, currentPage, isLoading: false };
  }).addCase(fetchMovies.rejected, (state: IMoviesState) => (
    { ...state, isLoading: false }
  ));
};

export default extraReducers;

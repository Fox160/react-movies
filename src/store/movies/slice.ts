import { createSlice } from '@reduxjs/toolkit';
import { IMoviesState } from '../types';
import extraReducers from './extraReducers';
import reducers from './reducers';

const initialState : IMoviesState = {
  list: [],
  currentPage: 1,
  isLoading: false,
};

const { actions, reducer } = createSlice({
  name: 'movies',
  initialState,
  reducers,
  extraReducers,
});

export const { setCurrentPage } = actions;

export default reducer;

import { IRootState } from '../types';

const selectMovies = (state: IRootState) => state.movies;

export default selectMovies;

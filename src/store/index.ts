import { configureStore, getDefaultMiddleware } from '@reduxjs/toolkit';
import { createLogger } from 'redux-logger';

import comments from './comments';
import movies from './movies';
import movieDetail from './moviesDetail';

const reducer = {
  movies,
  comments,
  movieDetail,
};

const store = configureStore({
  reducer,
  middleware: [...getDefaultMiddleware(), createLogger()],
});

export type AppDispatch = typeof store.dispatch;

export default store;

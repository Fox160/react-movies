import { IRootState } from '../types';

const selectComments = (state: IRootState) => state.comments;

export default selectComments;

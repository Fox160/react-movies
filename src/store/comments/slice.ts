import { createSlice } from '@reduxjs/toolkit';
import { v4 as uuidv4 } from 'uuid';
import { loremIpsum } from 'lorem-ipsum';
import randomDate from '../../helpers/randomDate';
import { IComment } from '../../components/DetailPage/CommentsItem/types';
import reducers from './reducers';

const commentsCount = 1000;

const initialState: IComment[] = Array(commentsCount).fill(0).map(() => (
  {
    id: uuidv4(),
    name: 'You',
    image: 'http://via.placeholder.com/40',
    text: loremIpsum({
      count: 1,
      units: 'sentences',
      sentenceLowerBound: 4,
      sentenceUpperBound: 8,
    }),
    date: randomDate(new Date(2010, 0, 1), new Date()),
    removable: true,
  }
));

const { actions, reducer } = createSlice({
  name: 'comments',
  initialState,
  reducers,
});

export const { removeComment } = actions;

export default reducer;

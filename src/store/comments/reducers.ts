import { IComment } from '../../components/DetailPage/CommentsItem/types';

const reducers = {
  removeComment: (state: IComment[], action:any) => {
    const itemId = action.payload;

    return state.filter((commentItem) => commentItem.id !== itemId);
  },
};

export default reducers;

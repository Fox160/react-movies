import React, { useContext, useEffect } from 'react';

import MoviesItem from '../MoviesItem/MoviesItem';

import PreloaderContext from '../../../context/preloaderContext';

import { IMovie } from '../MoviesItem/types';
import useAppSelector from '../../../hooks';

import selectMovies from '../../../store/movies/selectors';

import './_movies.scss';

const Movies = () => {
  const movies = useAppSelector(selectMovies);
  const { setPreloaderVisible } = useContext(PreloaderContext);

  useEffect(() => {
    setPreloaderVisible(movies.isLoading);
  }, [movies]);

  if (!movies.list.length) {
    return null;
  }

  return (
    <div className="movies container">
      <div className="movies__list">
        {movies.list.map((movie: IMovie) => (
          <MoviesItem
            movie={movie}
            className="movies__item"
            key={movie.id}
          />
        ))}
      </div>
    </div>
  );
};

export default Movies;

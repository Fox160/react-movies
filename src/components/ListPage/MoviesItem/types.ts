export interface IMovie {
  id: number;
  large_cover_image: string;
  title: string;
  year: string;
  genres?: string[];
  rating: number;
}

export interface IMovieProps {
  movie: IMovie;
  className?: string;
}

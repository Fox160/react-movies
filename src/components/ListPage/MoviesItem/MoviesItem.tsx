import React from 'react';
import { Link } from 'react-router-dom';

import './_movies-item.scss';
import Star from '../../../assets/img/star.svg';
import emptyImg from '../../../assets/img/poster-empty.jpg';

import { IMovieProps } from './types';
import Img from '../../common/Img';
import MoviesGenres from '../MoviesGenres';

const MoviesItem = ({ movie, className }: IMovieProps) => (
  <div className={`movies-item ${className}`}>
    <div className="movies-item__poster">
      <Img preloader={emptyImg} img={movie.large_cover_image} />

      <div className="movies-item__detail">
        <div className="movies-item__rating rating">
          <Star />
          {movie.rating}
        </div>

        {movie.genres && (
          <MoviesGenres
            data={movie.genres.slice(0, 3)}
            className="movies-item__genres-list"
            itemClass="movies-item__genre"
          />
        )}

        <Link to={`/${movie.id}`} className="movies-item__link">
          More
        </Link>
      </div>
    </div>
    <div className="movies-item__info">
      <p className="movies-item__title">{movie.title}</p>
      <p className="movies-item__year">{movie.year}</p>
    </div>
  </div>
);

export default MoviesItem;

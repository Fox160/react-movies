import React from 'react';
import { IProps } from './types';

const MoviesGenres: React.FC<IProps> = ({ data, className, itemClass }) => {
  if (!data) {
    return null;
  }

  return (
    <div className={`genres ${className}`}>
      {data.map((genre) => (
        <div key={`${genre}`} className={itemClass}>
          {genre}
        </div>
      ))}
    </div>
  );
};

export default MoviesGenres;

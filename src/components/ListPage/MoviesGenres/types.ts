export interface IProps {
  data?: string[],
  className: string,
  itemClass: string,
}

import React from 'react';

import styles from './Preloader.module.scss';

import { IPreloaderProps } from './types';

const Preloader = ({ isLoading } : IPreloaderProps) => {
  if (!isLoading) {
    return null;
  }

  return (
    <div className={styles.preloader}>
      <div className={styles.loader} />
    </div>
  );
};

export default Preloader;

import React from 'react';
import { useDispatch } from 'react-redux';
import useAppSelector from '../../../hooks';

import { fetchMovies } from '../../../store/movies/extraReducers';
import selectMovies from '../../../store/movies/selectors';

import './_pagination.scss';

const Pagination = () => {
  const movies = useAppSelector(selectMovies);
  const dispatch = useDispatch();

  const changePage = (page: number) => {
    dispatch(fetchMovies(page));
  };

  return (
    <div className="pagination">
      {movies.currentPage - 1 > 0 && (
        <button
          className="pagination__btn"
          type="button"
          onClick={() => changePage(movies.currentPage - 1)}
        >
          {movies.currentPage - 1}
        </button>
      )}
      <button type="button" className="pagination__btn pagination__btn--active">
        {movies.currentPage}
      </button>
      <button
        type="button"
        className="pagination__btn"
        onClick={() => changePage(movies.currentPage + 1)}
      >
        {movies.currentPage + 1}
      </button>
    </div>
  );
};

export default Pagination;

import React from 'react';
import { withRouter } from 'react-router-dom';

import useAppSelector from '../../../hooks';

import selectMovieDetail from '../../../store/moviesDetail/selectors';

import HeaderBack from './HeaderBack';
import Pagination from '../Pagination';

import './_header.scss';

const Header = () => {
  const movieDetail = useAppSelector(selectMovieDetail);

  return (
    <header className="header">
      <div className="container">
        <span>{movieDetail.info.title || 'React Movies'}</span>
        <span>{movieDetail.info.title ? <HeaderBack /> : <Pagination />}</span>
      </div>
    </header>
  );
};

export default withRouter(Header);

import React from 'react';
import { useHistory } from 'react-router-dom';
import { useDispatch } from 'react-redux';

import Undo from '../../../assets/img/undo.svg';

import { clearMovieDetail } from '../../../store/moviesDetail/slice';

const HeaderBack = () => {
  const history = useHistory();
  const dispatch = useDispatch();

  const goBack = () => {
    history.goBack();
    dispatch(clearMovieDetail());
  };

  return (
    <button type="button" className="button" onClick={goBack}>
      <Undo />
    </button>
  );
};

export default HeaderBack;

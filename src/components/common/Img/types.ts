export interface IProps {
  preloader?: string;
  img?: string;
  alt?: string;
}

import React, { useState } from 'react';

import { IProps } from './types';

import emptyImg from '../../../assets/img/poster-empty.jpg';

const Img: React.FC<IProps> = ({ preloader = emptyImg, img, alt }) => {
  const [isLoaded, setIsLoaded] = useState<boolean>(false);

  const bg = new Image();
  bg.src = img || '';
  bg.onload = () => {
    setIsLoaded(true);
  };

  return <img src={isLoaded ? img : preloader} alt={alt || 'img'} />;
};

export default Img;

import { RouteComponentProps } from 'react-router';

export interface IMovieDetail {
  title?: string,
  genres?: string[],
  large_cover_image?: string,
  rating?: number,
  year?: number,
  description_full?: string,
}

interface RouterProps {
  movieId: string;
}

export interface IMovieDetailProps extends RouteComponentProps<RouterProps> {}

import React, {
  useContext, useEffect,
} from 'react';
import { useDispatch } from 'react-redux';

import Comments from '../Comments/Comments';
import Chart from '../Chart/Chart';
import MoviesGenres from '../../ListPage/MoviesGenres';
import Img from '../../common/Img';

import selectMovieDetail from '../../../store/moviesDetail/selectors';
import { fetchMovieDetail } from '../../../store/moviesDetail/extraReducers';

import PreloaderContext from '../../../context/preloaderContext';

import { IMovieDetailProps } from './types';
import useAppSelector from '../../../hooks';

import './_movie-detail.scss';
import Star from '../../../assets/img/star.svg';

const MovieDetail: React.FC<IMovieDetailProps> = ({ match }) => {
  const { movieId } = match.params;
  const movie = useAppSelector(selectMovieDetail);
  const { setPreloaderVisible } = useContext(PreloaderContext);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchMovieDetail(movieId));
  }, []);

  useEffect(() => {
    setPreloaderVisible(movie.isLoading);
  }, [movie]);

  if (Object.keys(movie.info).length === 0) {
    return (
      <div className="movie-detail container">
        Movie Not Found
      </div>
    );
  }

  return (
    <div className="movie-detail container">
      <div className="movie-detail__poster">
        <Img img={movie.info.large_cover_image} alt={movie.info.title} />
        <div className="movie-detail__rating rating">
          <Star />
          {movie.info.rating}
        </div>
      </div>

      <div className="movie-detail__info">
        <p className="movie-detail__title">{movie.info.title}</p>
        <p className="movie-detail__year">{movie.info.year}</p>

        <MoviesGenres
          data={movie.info.genres}
          className="movie-detail__genres-list"
          itemClass="movie-detail__genre"
        />

        <div className="movie-detail__synopsis">
          <h2>Synopsis</h2>
          {movie.info.description_full}
        </div>

        <div className="movie-detail__comments">
          <h2>Comments</h2>
          <Comments />
        </div>
      </div>
      <Chart
        top={10}
        right={50}
        bottom={50}
        left={50}
        height={400}
        className="movie-detail__chart"
      />
    </div>
  );
};

export default MovieDetail;

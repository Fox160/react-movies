import React, { useEffect, useRef } from 'react';
import * as d3 from 'd3';

import './Chart.scss';

import { Data, IChartProps } from './types';

const CHART_DATA = [
  { xData: 'jQuery', value: 43.3 },
  { xData: 'React.js', value: 35.9 },
  { xData: 'Angular', value: 25.1 },
  { xData: 'ASP.NET', value: 21.9 },
  { xData: 'Express', value: 21.2 },
  { xData: '.NET Core', value: 19.1 },
  { xData: 'Vue.js', value: 17.3 },
  { xData: 'Spring', value: 16.4 },
  { xData: 'Angular.js', value: 16.1 },
  { xData: 'Django', value: 14.2 },
  { xData: 'Flask', value: 14.2 },
  { xData: 'Laravel', value: 11.1 },
  { xData: 'Rails', value: 7.0 },
  { xData: 'Symfony', value: 14.4 },
  { xData: 'Gatsby', value: 4.0 },
  { xData: 'Drupal', value: 3.2 },
];

const Chart = ({
  height, top, right, bottom, left, data = CHART_DATA, className,
}: IChartProps) => {
  const containerRef = useRef<HTMLDivElement>(null);

  const draw = () => {
    if (containerRef.current) {
      const width = containerRef.current.clientWidth;

      const svgWidth = width - left - right;
      const svgHeight = height - top - bottom;

      const x = d3
        .scaleBand()
        .domain(data.map((d) => d.xData))
        .range([0, svgWidth])
        .padding(0.1);
      const y = d3
        .scaleLinear()
        .domain([
          0,
          d3.max(data, () => Math.max(...data.map((dt) => (dt as Data).value), 0)),
        ] as number[])
        .range([svgHeight, 0]);

      const svg = d3
        .select('.chart')
        .append('svg')
        .attr('width', svgWidth + left + right)
        .attr('height', svgHeight + top + bottom)
        .append('g')
        .attr('transform', `translate(${left},${top})`);

      svg
        .selectAll('.chart__bar')
        .data(data)
        .enter()
        .append('rect')
        .attr('class', 'chart__bar')
        .attr('x', (d) => x(d.xData) || 0)
        .attr('width', x.bandwidth())
        .attr('y', (d) => y(d.value))
        .attr('height', (d) => svgHeight - y(d.value));

      svg.append('g').attr('transform', `translate(0,${svgHeight})`).call(d3.axisBottom(x));

      svg.append('g').call(d3.axisLeft(y));
    }
  };

  useEffect(() => {
    draw();
  }, []);

  return <div className={`chart ${className}`} ref={containerRef} />;
};

export default Chart;

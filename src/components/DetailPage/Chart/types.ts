export interface IChartProps {
  height: number;
  top: number;
  right: number;
  bottom: number;
  left: number;
  data?: Data[];
  className?: string;
}

export type Data = {
  xData: string;
  value: number;
};

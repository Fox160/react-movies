import React from 'react';
import {
  AutoSizer, CellMeasurer, CellMeasurerCache, List, ListRowProps,
} from 'react-virtualized';
import { useDispatch } from 'react-redux';

import CommentsItem from '../CommentsItem';

import selectComments from '../../../store/comments/selectors';
import { removeComment } from '../../../store/comments/slice';

import { IComment } from '../CommentsItem/types';

import useAppSelector from '../../../hooks';

import './_comments.scss';

const Comments = () => {
  const data = useAppSelector(selectComments);
  const dispatch = useDispatch();

  const cache = new CellMeasurerCache({
    fixedWidth: true,
    defaultHeight: 100,
  });

  const renderRow = ({
    index, key, parent, style,
  }: ListRowProps): JSX.Element => {
    const comment: IComment = data[index];

    return (
      <CellMeasurer
        key={key}
        cache={cache}
        parent={parent}
        columnIndex={0}
        rowIndex={index}
      >
        <div style={style}>
          <CommentsItem
            comment={comment}
            key={comment.id}
            handleRemove={() => dispatch(removeComment(comment.id))}
            className="comments__item"
          />
        </div>
      </CellMeasurer>
    );
  };

  if (!data.length) {
    return null;
  }

  return (
    <div className="comments">
      <div className="comments__list">
        <AutoSizer>
          {({ width, height }) => (
            <List
              width={width}
              height={height}
              deferredMeasurementCache={cache}
              rowHeight={cache.rowHeight}
              rowRenderer={renderRow}
              rowCount={data.length}
            />
          )}
        </AutoSizer>
      </div>
    </div>
  );
};

export default Comments;

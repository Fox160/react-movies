import React from 'react';

import RemoveIcon from '../../../assets/img/delete-cross.svg';
import './_comments-item.scss';

import { ICommentProps } from './types';

const CommentsItem = ({ comment, handleRemove, className }: ICommentProps) => {
  if (!comment) {
    return null;
  }

  return (
    <div className={`comments-item ${className}`}>
      <div className="comments-item__head">
        <div className="comments-item__image">
          <img src={comment.image} alt="" />
        </div>
        <span>{comment.name}</span>
        <span className="comments-item__date">{comment.date}</span>
        {comment.removable ? (
          <button type="button" className="button comments-item__remove" onClick={handleRemove}>
            <RemoveIcon />
          </button>
        ) : null}
      </div>

      <div className="comments-item__text">{comment.text}</div>
    </div>
  );
};

export default CommentsItem;

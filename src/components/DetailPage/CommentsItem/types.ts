export interface ICommentProps {
  comment: IComment;
  handleRemove?: () => void;
  className?: string;
}

export interface IComment {
  id: string;
  name?: string;
  date?: Date | string;
  text?: string;
  removable?: boolean;
  image?: string;
}

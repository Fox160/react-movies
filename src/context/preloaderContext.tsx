/* eslint-disable @typescript-eslint/no-unused-vars */
import React from 'react';

const PreloaderContext = React.createContext({
  preloaderVisible: true,
  setPreloaderVisible: (flag: boolean) => {},
});

export default PreloaderContext;

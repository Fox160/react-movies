import { TypedUseSelectorHook, useSelector } from 'react-redux';
import { IRootState } from '../store/types';

const useAppSelector: TypedUseSelectorHook<IRootState> = useSelector;

export default useAppSelector;

import api from '../utils/api';

export const getMovies = (page) => (
  api
    .get('/list_movies.json', {
      params: {
        limit: process.env.MOVIES_LIMIT_ON_PAGE,
        sort_by: 'rating',
        page,
      },
    }).then(({ data }) => {
      if (data?.data) {
        return data.data.movies;
      }

      return null;
    })
);

export const getMovieDetail = (movieId) => (
  api
    .get('/movie_details.json', {
      params: {
        movie_id: movieId,
      },
    }).then(({ data }) => {
      if (data?.data) {
        return data.data.movie;
      }

      return null;
    })
);

export default api;

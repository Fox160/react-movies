import React, { useState, useEffect } from 'react';
import { HashRouter, Route, Switch } from 'react-router-dom';
import { useDispatch } from 'react-redux';

import MovieDetail from './components/DetailPage/MovieDetail';
import Movies from './components/ListPage/Movies';
import Header from './components/common/Header';
import Preloader from './components/common/Preloader/Preloader';

import PreloaderContext from './context/preloaderContext';
import { fetchMovies } from './store/movies/extraReducers';

const App = () => {
  const [preloaderVisible, setPreloaderVisible] = useState<boolean>(true);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchMovies());
  }, []);

  return (
    <>
      <HashRouter>
        <PreloaderContext.Provider value={{ preloaderVisible, setPreloaderVisible }}>
          <Preloader isLoading={preloaderVisible} />
          <Header />
          <Switch>
            <Route path="/" exact component={Movies} />
            <Route path="/:movieId" exact component={MovieDetail} />
          </Switch>
        </PreloaderContext.Provider>
      </HashRouter>
    </>
  );
};

export default App;

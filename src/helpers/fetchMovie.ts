import { getMovies } from '../api';

const fetchMovieHelper = async (page:number = 1) => {
  const response = await getMovies(page);

  return { list: response, currentPage: page };
};

export default fetchMovieHelper;

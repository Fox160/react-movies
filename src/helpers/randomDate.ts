const randomDate = (start: Date, end: Date) => {
  const res = new Date(start.getTime() + Math.random() * (end.getTime() - start.getTime()));

  return res.toLocaleString();
};

export default randomDate;

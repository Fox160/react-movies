import { getMovieDetail } from '../api';

const fetchMovieDetailHelper = async (movieId: string) => {
  const response = await getMovieDetail(movieId);

  return response;
};

export default fetchMovieDetailHelper;
